from django.db import models
from django.shortcuts import reverse
from django.contrib.auth.models import User
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='static/img')
    def __str__(self):
        return self.user.__str__()
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

class Rating(models.Model):
    rate = models.IntegerField()
    def __int__(self):
        return self.rate
    class Meta:
        verbose_name = 'Оценка'
        verbose_name_plural = 'Оценки'

class Question(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    tags = models.ManyToManyField('Tag', blank=True, related_name='posts')
    date_pub = models.DateField(auto_now_add=True)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

class Tag(models.Model):
    title = models.CharField(max_length=50, unique = True)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

class Answer(models.Model):
    text = models.TextField()
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    correctness = models.BooleanField()
    date_pub = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.text
    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'