from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger  
from django.shortcuts import render
from app.models import Profile, Tag, Rating, Question, Answer
from django.core.paginator import Paginator
from django.http import Http404
import datetime

def index(request):
    post_list= Question.objects.order_by('-id')[:9]
    paginator = Paginator(post_list, 3) # Show 25 contacts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)
    return render(request, 'index.html', {'questions': posts, 'posts': posts})
    


def hot_questions(request):
    now = datetime.date.today()
    post_list= Question.objects.all().filter(date_pub = now)
    paginator = Paginator(post_list, 3) # Show 25 contacts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)
    return render(request, 'hot_questions.html', {'questions': posts, 'posts': posts})
    


def ask(request):
    return render(request, 'ask.html', {})

def question(request, slug):
    question = Question.objects.filter(id__iexact=slug)
    if (question.count() == 0):
        raise Http404
    answer = Answer.objects.all().filter(question__id__in = [slug]).count()    
    answer_list = Answer.objects.all().filter(question__id__in = [slug])
    paginator = Paginator(answer_list, 3) # Show 25 contacts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)
    today = datetime.date.today()
    return render(request, 'question.html', {'questions': question, 'posts': posts, 'answers': answer})


def tag(request, tit):
    tags = Tag.objects.filter(title__iexact=tit)
    if (tags.count() == 0):
        raise Http404
    post_list= Question.objects.filter(tags__title__in= [tit])
    paginator = Paginator(post_list, 3) # Show 25 contacts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)
    return render(request, 'tag.html', {'tags': tags, 'posts': posts})


def settings(request):
    return render(request, 'settings.html', {})

def login(request):
    return render(request, 'login.html', {})

def register(request):
    return render(request, 'register.html', {})
