from django.contrib import admin

# Register your models here.
from app.models import Profile, Tag, Rating, Question, Answer

admin.site.register(Profile)
admin.site.register(Tag)
admin.site.register(Rating)
admin.site.register(Question)
admin.site.register(Answer)